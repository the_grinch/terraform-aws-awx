### The Ansible inventory file
resource "local_file" "AnsibleInventory" {
 content = templatefile("inventory.tmpl",
 {
  private-dns = aws_instance.awx.*.private_dns,
  private-ip = aws_instance.awx.*.private_ip,
  private-id = aws_instance.awx.*.id
 }
 )
 filename = "inventory"
}
